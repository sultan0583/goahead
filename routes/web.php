<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/services', function () {
	return view('pages.services');
});

Route::get('about', function () {
	return view('pages.about');
});

Route::get('gallery', function () {
	return view('pages.gallery');
});

Route::get('mission', function () {
	return view('pages.mission');
});

Route::get('contact', function () {
	return view('pages.contact');
});

Route::get('lang', function () {

	if(! is_null(session('lang')) && session('lang') == 'en') {
		session()->put('lang', 'ar');
	} else {
		session()->put('lang', 'en');
	}

	return redirect('/');

});