 @extends('layouts.master')

 @section('header')
    @include('layouts._pages_header')
 @endsection

 @section('content')

 <main class="page-content">
        <section class="section-border text-center text-md-left">
          <div class="container">
            <ol class="breadcrumb">
              <li><a href="index.html">Home</a></li>
              <li class="active">Gallery</li>
            </ol>
          </div>
        </section>

        <!--Start section-->
        <section class="text-center well well-sm">
          <div class="container-fluid no-gutter-md">
            <div class="row">
              <div class="col-xs-12">
                <h1>Cobbles Grid</h1>
                <div class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing eli</div>
              </div>
              <div class="col-xs-12 offset-1">
                <div class="btn-group-isotope">
                  <button data-isotope-filter="*" data-isotope-group="gallery" class="btn btn-default btn-xs round-xl">All</button>
                  <button data-isotope-filter="type-1" data-isotope-group="gallery" class="btn btn-default btn-xs round-xl"> Type 1</button>
                  <button data-isotope-filter="type-2" data-isotope-group="gallery" class="btn btn-default btn-xs round-xl"> Type 2</button>
                  <button data-isotope-filter="type-3" data-isotope-group="gallery" class="btn btn-default btn-xs round-xl"> Type 3</button>
                </div>
              </div>
            </div>
            <div data-isotope-layout="masonry" data-isotope-group="gallery" data-lightbox="gallery" class="isotope offset-1 isotope-no-gutter">
              <div data-filter="type-1" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-8_original.jpg" data-lightbox="image"><img src="images/gallery-22.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-1" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-9_original.jpg" data-lightbox="image"><img src="images/gallery-23.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-1" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-24_original.jpg" data-lightbox="image"><img src="images/gallery-24.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-2" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-11_original.jpg" data-lightbox="image"><img src="images/gallery-25.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-2" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-2_original.jpg" data-lightbox="image"><img src="images/gallery-26.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-2" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-7_original.jpg" data-lightbox="image"><img src="images/gallery-27.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-12_original.jpg" data-lightbox="image"><img src="images/gallery-28.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_40 text-center isotope-item"><a href="images/gallery-13_original.jpg" data-lightbox="image"><img src="images/gallery-29.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-3_original.jpg" data-lightbox="image"><img src="images/gallery-30.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-14_original.jpg" data-lightbox="image"><img src="images/gallery-31.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-4_original.jpg" data-lightbox="image"><img src="images/gallery-32.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-1_original.jpg" data-lightbox="image"><img src="images/gallery-33.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-15_original.jpg" data-lightbox="image"><img src="images/gallery-34.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-5_original.jpg" data-lightbox="image"><img src="images/gallery-35.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
              <div data-filter="type-3" class="thumbnail-variant-2 thumbnail-4_col10 width_20 text-center isotope-item"><a href="images/gallery-10_original.jpg" data-lightbox="image"><img src="images/gallery-36.jpg" alt="">
                  <div class="caption">
                    <h4 class="text-white">Quality Products for Companies<small>COMPANY NEWS,  GENERAL NEWS</small></h4>
                  </div></a><a href="#" class="icon icon-sm text-white fa-chain"></a></div>
            </div>
          </div>
        </section>
        <!--End section-->
        
        
      </main>

@endsection