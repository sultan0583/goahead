@extends('layouts.master')

@section('header')
	@include('layouts._pages_header')
@endsection

@section('content')
  <main class="page-content">
        <ol class="breadcrumb section-border bg-lighter">
          <li><a href="index.html">Home</a></li>
          <li><a href="#">Elements</a></li>
          <li class="active">Contact us</li>
        </ol>

        <!--Start section-->
        <section class="well well-sm bg-lighter">
          <div class="container">
            <div class="row flow-offset-1"> 
              <div class="col-md-4 text-center text-md-left">
                <address class="contact-block inset-sm-min-2">
                  <dl>
                    <dt class="h6">ADDRESS</dt>
                    <dd>123, Nasr City, Cairo </dd>
                    <dt class="h6">WE ARE OPEN</dt>
                    <dd>Open hours: 8.00-18.00 Mon-Sat</dd>
                    <dt class="h6">PHONE</dt>
                    <dd><a href="callto:#">123-456-7890, &nbsp;</a><a href="callto:#">098-765-4321</a></dd>
                    <dt class="h6">E-MAIL</dt>
                    <dd><a href="mailto:#">info@goahead.com.eg</a></dd>
                  </dl>
                  <ul class="list-inline list-inline-3">
                    <li><a href="#" class="fa-facebook-square"></a></li>
                    <li><a href="#" class="fa-twitter-square"></a></li>
                    <li><a href="#" class="fa-google-plus-square"></a></li>
                  </ul>
                </address>
              </div>
              <div class="col-md-8 btn-shadow inset-sm-min bg-white">
                <h5 class="text-center">GET IN TOUCH</h5>
                <form method="post" action="bat/rd-mailform.php" class="row label-insets rd-mailform">
                  <!--RD Mailform Type-->
                  <input type="hidden" name="form-type" value="contact">
                  <!--END RD Mailform Type-->
                  <div class="form-group col-sm-6">
                    <label for="exampleInputText1" class="text-uppercase font-secondary form-label">First Name</label>
                    <input type="text" placeholder="Your First Name" name="name" data-constraints="@NotEmpty @LettersOnly" id="exampleInputText1" class="form-control">
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="exampleInputText2" class="text-uppercase font-secondary">	Last Name</label>
                    <input type="text" placeholder="Your Last Name" name="name2" data-constraints="@NotEmpty @LettersOnly" id="exampleInputText2" class="form-control">
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="exampleTextarea" class="text-uppercase font-secondary">your message</label>
                    <textarea id="exampleTextarea" rows="3" placeholder="Write your message here" name="message" data-constraints="@NotEmpty" class="form-control"></textarea>
                  </div>
                  <div class="form-group col-sm-6">
                    <label for="exampleInputEmail1" class="text-uppercase font-secondary">E-mail</label>
                    <input placeholder="E-mail" type="text" name="email" data-constraints="@NotEmpty @Email" id="exampleInputEmail1" class="form-control">
                  </div>
                  <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary btn-xs round-xl btn-block form-el-offset-1">Send message</button>
                    <div class="mfInfo"></div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <!--End section-->
        <section class="map">
          <div id="google-map" class="map_model"></div>
          <ul class="map_locations">
            <li data-x="30.0594699" data-y="31.1884236" data-basic="images/gmap_marker.png"
            	 data-active="images/gmap_marker_active.png">
              <p>9870 St Nasr City, Cairo <span>800 2345-6789</span></p>
            </li>
          </ul>
        </section>
        
      </main>
@endsection