@extends('layouts.master')


@section('header')
	@include('layouts._pages_header')
@endsection

@section('content')

 <main class="page-content">
        <ol class="breadcrumb section-border">
          <li><a href="index.html">Home</a></li>
          <li><a href="#">Services</a></li>
          <li class="active">Service 1</li>
        </ol>

        <!--Start section-->
        <section class="text-center text-sm-left well well-sm section-border">
          <div class="container">
            <div class="row">
              <div class="col-lg-8 col-lg-offset-2 bg-white text-center">
                <h1>Our Main Services</h1>
                <p class="lead">We are the highest value provider of global construction services and technical expertise</p>
              </div>
            </div>
            <div class="row offset-1 flow-offset-3">
              <div class="col-sm-6">
                <div class="box-sm box-skin-1 bg-lighter box-skin-left-offset-negative">
                  <div class="box__left box-md-inset-1"><span class="icon icon-md icon-primary line-height-1 material-icons-thumb_up"></span></div>
                  <div class="box__body box__middle">
                    <h5><a href="#">Loremim Ipsumaes</a></h5>
                    <p>
                       Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad, 
                eam ei debet iriure patrioque id ...
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="box-sm box-skin-1 bg-lighter box-skin-right-offset-negative">
                  <div class="box__left box-md-inset-1"><span class="icon icon-md icon-primary line-height-1 material-icons-business"></span></div>
                  <div class="box__body box__middle">
                    <h5><a href="#">Loremim Ipsumaes</a></h5>
                    <p>
                       Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad, 
                eam ei debet iriure patrioque id ...
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="box-sm box-skin-1 bg-lighter box-skin-left-offset-negative">
                  <div class="box__left box-md-inset-1"><span class="icon icon-md icon-primary line-height-1 material-icons-build"></span></div>
                  <div class="box__body box__middle">
                    <h5><a href="#">Loremim Ipsumaes</a></h5>
                    <p>
                       Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad, 
                eam ei debet iriure patrioque id ...
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="box-sm box-skin-1 bg-lighter box-skin-right-offset-negative">
                  <div class="box__left box-md-inset-1"><span class="icon icon-md icon-primary line-height-1 material-icons-wb_incandescent"></span></div>
                  <div class="box__body box__middle">
                    <h5><a href="#">Loremim Ipsumaes</a></h5>
                    <p>
                      Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad, 
                eam ei debet iriure patrioque id ...
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--End section-->

  	</main>
@endsection