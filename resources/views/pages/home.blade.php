@extends('layouts.master')

@section('header')
  @include('layouts._header')
@endsection

@section('content')
<main class="page-content">
  <!--Start section-->
  <section class="text-center well well-sm text-md-left section-border">
    <div class="container">
      <div class="row flow-offset-2">
        <div class="col-md-4">
          <h4>OUR MISSION</h4>
          <hr class="short bg-primary">
          <p>We aim to make clients happy by selling the best products at the best prices, in a friendly, fun atmosphere. Our rating has remained at 100% with excellent and positive feedbacks. We are constantly adding new products.</p>
        </div>
        <div class="col-md-4">
          <h4>WHAT WE DO</h4>
          <hr class="short bg-primary">
          <p>
            With teams of experienced professionals we give multiple solutions to our clients around the world. Our professionals deliver high-touch services to help clients every step they take.

          </p>
        </div>
        <div class="col-md-4">
          <h4>WHY CHOOSE US</h4>
          <hr class="short bg-primary">
          <p>
            We have five values that have always contributed to our success; take pride, be passionate, never give up, work as a team and make it fun.


          </p>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

  <!--Start section-->
  <section class="bg-primary text-center">
    <div class="container counter-panel">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-3">
          <div data-from="0" data-to="10" class="counter"></div>
          <p class="text-bold text-uppercase">YEARS OF EXPERIENCE</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3">
          <div data-from="0" data-to="7" class="counter"></div>
          <p class="text-bold text-uppercase">LOREM IPSUM</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3">
          <div data-from="0" data-to="26" class="counter"></div>
          <p class="text-bold text-uppercase">LOREM IPSUM</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3">
          <div data-from="0" data-to="12" class="counter"></div>
          <p class="text-bold text-uppercase">LOREM IPSUM</p>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

  <!--Start section-->
  <section class="text-center text-sm-left well well-sm">
          <div class="container">
            <div class="row flow-offset-1">
              <div class="col-sm-4"><span class="icon icon-xxl icon-primary material-icons-thumb_up"></span>
                <h5><a href="#">Loremim Ipsumaes</a></h5>
                <p>
                Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad, 
                eam ei debet iriure patrioque id ...
                </p>
              </div>
              <div class="col-sm-4"><span class="icon icon-xxl icon-primary material-icons-business"></span>
                <h5><a href="#">Loremim Ipsumaes</a></h5>
                <p>
                  Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad,
                   eam ei debet iriure patrioque id ...
                </p>
              </div>
              <div class="col-sm-4"><span class="icon icon-xxl icon-primary material-icons-build"></span>
                <h5><a href="#">Loremim Ipsumaes</a></h5>
                <p>
                   Lorem ipsum dolor sit amet, mollis accusam ne sed. Eum modus lorem libris ad,
                   eam ei debet iriure patrioque id ...                  
                </p>
              </div>
            </div>
          </div>
        </section>
  <!--End section-->

  <!--Start section progress Bar Radial-->
  <section class="text-center well bg-lighter">
    <div class="container">
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-3 inset-vw">
          <div data-value="73" data-stroke="5" data-trail="3" data-easing="linear" data-counter="true" data-duration="1000" class="progress-bar-custom progress-bar-radial progress-bar-secondary-1"></div>
          <p class="font-secondary big text-uppercase text-light-clr">Leadership</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 inset-vw">
          <div data-value="89" data-stroke="5" data-trail="3" data-easing="linear" data-counter="true" data-duration="1000" class="progress-bar-custom progress-bar-radial progress-bar-secondary-1"></div>
          <p class="font-secondary big text-uppercase text-light-clr">Management</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 inset-vw">
          <div data-value="95" data-stroke="5" data-trail="3" data-easing="linear" data-counter="true" data-duration="1000" class="progress-bar-custom progress-bar-radial progress-bar-secondary-1"></div>
          <p class="font-secondary big text-uppercase text-light-clr">Innovation and creativity</p>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-3 inset-vw">
          <div data-value="80" data-stroke="5" data-trail="3" data-easing="linear" data-counter="true" data-duration="1000" class="progress-bar-custom progress-bar-radial progress-bar-secondary-1"></div>
          <p class="font-secondary big text-uppercase text-light-clr">Resilience</p>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

  <!--Start section-->
  <section class="well well-sm well-inset-2 text-center text-md-left">
    <div class="container">
      <h1 class="text-center">Latest From The Blog</h1>
      <div class="row flow-offset-2">
        <div class="col-sm-6 col-md-4">
          <article class="thumbnail thumbnail-4 slow-hover">
            <div class="image-slow-wrapper"><img src="images/soltan/learning-1.jpg" alt=""></div>
            <div class="caption">
              <h4><a href="blog_post.html">Lorem ipsum dolor sit amet</a></h4>
              <p class="text-dark-variant-2">
                Lorem ipsum dolor sit amet, mollis accusam ne sed. 
                Eum modus lorem libris ad, eam ei debet iriure patrioque id...
              </p>
              <div class="blog-info">
                <div class="pull-md-left">
                  <time datetime="2015" class="meta material-icons-schedule">Feb 11, 2016</time><a href="#" class="badge material-icons-chat_bubble_outline font-secondary">13</a>
                </div><a href="blog_post.html" class="btn-link text-bold">Read More</a>
              </div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="thumbnail thumbnail-4 slow-hover">
            <div class="image-slow-wrapper"><img src="images/soltan/learning-4.jpg" alt=""></div>
            <div class="caption">
              <h4><a href="blog_post.html">Lorem ipsum dolor sit amet</a></h4>
              <p class="text-dark-variant-2">
                Lorem ipsum dolor sit amet, mollis accusam ne sed. 
                Eum modus lorem libris ad, eam ei debet iriure patrioque id...
              </p>
              <div class="blog-info">
                <div class="pull-md-left">
                  <time datetime="2015" class="meta material-icons-schedule">Feb 03, 2016</time><a href="#" class="badge material-icons-chat_bubble_outline font-secondary">13</a>
                </div><a href="blog_post.html" class="btn-link text-bold">Read More</a>
              </div>
            </div>
          </article>
        </div>
        <div class="col-sm-6 col-md-4">
          <article class="thumbnail thumbnail-4 slow-hover">
            <div class="image-slow-wrapper"><img src="images/soltan/learning-3.jpg" alt=""></div>
            <div class="caption">
              <h4><a href="blog_post.html">Lorem ipsum dolor sit amet</a></h4>
              <p class="text-dark-variant-2">
                  Lorem ipsum dolor sit amet, mollis accusam ne sed. 
                Eum modus lorem libris ad, eam ei debet iriure patrioque id...
              </p>
              <div class="blog-info">
                <div class="pull-md-left">
                  <time datetime="2015" class="meta material-icons-schedule">Jan 27, 2016</time><a href="#" class="badge material-icons-chat_bubble_outline font-secondary">13</a>
                </div><a href="blog_post.html" class="btn-link text-bold">Read More</a>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

  <!--Start section-->
  {{-- <section class="text-center well well-sm bg-lighter">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <h1>Choose Your Plan</h1>
          <p class="lead">
            Construction. offers a premium product and service for
            the increasing number of premier customers
          </p>
        </div>
      </div>
      <div class="row offset-1 flow-offset-2">
        <div class="col-sm-6 col-lg-3">
          <div class="thumbnail thumbnail-3 pricing-box pricing-box-3 bg-white">
            <h6 class="text-uppercase text-light-clr letter-spacing-1">economy</h6>
            <h2 class="letter-spacing-1"><sup>$</sup>20<sub>/mo</sub></h2>
            <div class="caption">
              <ul class="list-unstyled">
                <li>Public presentation</li>
                <li>Core features</li>
                <li>Email support</li>
                <li>Lifetime membership</li>
                <li><a href="#" class="link">Get started</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="thumbnail thumbnail-3 pricing-box pricing-box-3 bg-white">
            <h6 class="text-uppercase text-light-clr letter-spacing-1">silver</h6>
            <h2 class="letter-spacing-1"><sup>$</sup>143<sub>/mo</sub></h2>
            <div class="caption">
              <ul class="list-unstyled">
                <li>Public presentation</li>
                <li>Core features</li>
                <li>Email support</li>
                <li>Lifetime membership</li>
                <li><a href="#" class="link">Get started</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="thumbnail thumbnail-3 pricing-box pricing-box-3 bg-white">
            <h6 class="text-uppercase text-light-clr letter-spacing-1">gold</h6>
            <h2 class="letter-spacing-1"><sup>$</sup>170<sub>/mo</sub></h2>
            <div class="caption">
              <ul class="list-unstyled">
                <li>Public presentation</li>
                <li>Core features</li>
                <li>Email support</li>
                <li>Lifetime membership</li>
                <li><a href="#" class="link">Get started</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="thumbnail thumbnail-3 pricing-box pricing-box-3 bg-white">
            <h6 class="text-uppercase text-light-clr letter-spacing-1">platinum</h6>
            <h2 class="letter-spacing-1"><sup>$</sup>419<sub>/mo</sub></h2>
            <div class="caption">
              <ul class="list-unstyled">
                <li>Public presentation</li>
                <li>Core features</li>
                <li>Email support</li>
                <li>Lifetime membership</li>
                <li><a href="#" class="link">Get started</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> --}}
  <!--End section-->



  <!--Start section-->
  <section class="well well-sm well-inset-3 bg-image bg-image-1 text-center bg-image-1--mod-1">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <h1>Subscribe to Our Newsletter</h1>
          <p class="lead">Sign up to our newsletter subscription and be the first to know about latest company news, special offers and discounts.</p>
          <form class="form-width-1 offset-1">
            <div class="form-group">
              <input type="email" placeholder="Name@domainname.com" id="exampleInputEmail1" class="width-1 form-control">
              <button type="submit" class="btn btn-primary btn-xs round-xl">Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

  <!--Start section-->
  <section class="well well-sm text-center section-border">
    <div class="container">
      <h1 class="text-center">Our Clients</h1>
      <ul class="flex-list offset-1">
        <li><a href="#"><img src="images/partner1.png" alt=""></a></li>
        <li><a href="#"><img src="images/partner3.png" alt=""></a></li>
        <li><a href="#"><img src="images/partner4.png" alt=""></a></li>
        <li><a href="#"><img src="images/partner5.png" alt=""></a></li>
        <li><a href="#"><img src="images/partner6.png" alt=""></a></li>
      </ul>
    </div>
  </section>
  <!--End section-->


  <!--Start section-->
  <section class="well well-sm well-inset-2 text-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
          <h1 class="text-center">Testimonials</h1>
          <p class="lead">
            Have a look at some of our clients' testimonials as well as what
            some of the suppliers say
          </p>
        </div>
      </div>
      <div class="row offset-1">
        <!--Owl Carousel-->
        <div data-items="1" data-sm-items="2" data-xs-items="1" data-md-items="3" data-nav="true" data-margin="30" class="owl-carousel">
          <div class="owl-item">
            <blockquote class="quote-2"><img src="images/index_img5.jpg" alt="" class="img-circle">
              <h6>
                <cite>Jennifer Rogers</cite>
              </h6>
              <p class="small--mod-1 text-light-clr text-uppercase">Creative Director</p>
              <p class="h6 text-base">
                <q>It's not the first time that I deal with the company. The top-notch quality of their services impressed me from the first time I reached them. The team of true professionals helped me with the decision-making a lot...</q>
              </p>
            </blockquote>
          </div>
          <div class="owl-item">
            <blockquote class="quote-2"><img src="images/index_img6.jpg" alt="" class="img-circle">
              <h6>
                <cite>Walter Williams</cite>
              </h6>
              <p class="small--mod-1 text-light-clr text-uppercase">Social Media Specialist</p>
              <p class="h6 text-base">
                <q>As soon as I enabled your service, I saw a significant improvement. It cut my working time in half. This is incredible!</q>
              </p>
            </blockquote>
          </div>
          <div class="owl-item">
            <blockquote class="quote-2"><img src="images/index_img7.jpg" alt="" class="img-circle">
              <h6>
                <cite>Derrick Whitehead</cite>
              </h6>
              <p class="small--mod-1 text-light-clr text-uppercase">Learning & Development Manager</p>
              <p class="h6 text-base">
                <q>Your company understands today’s requirements, but that’s not what makes you stand apart. You also know today’s business world and how to keep customers happy. That is why you win.</q>
              </p>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--End section-->

</main>
@endsection
