  <header class="page-header subpage_header">

        <!--RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar top-panel-none-items" data-layout="rd-navbar-fixed" data-hover-on="false" data-stick-up="false" data-sm-layout="rd-navbar-fullwidth" data-md-layout="rd-navbar-static">
            <div class="rd-navbar-top-panel">
              <div class="rd-navbar-inner">
                <button data-rd-navbar-toggle=".list-inline, .fa-envelope, .fa-phone, .material-icons-language" class="rd-navbar-collapse-toggle"><span></span></button><a href="mailto:#" class="fa-envelope">demomail@demolink.org</a><a href="callto:#" class="fa-phone">+1 (126) 598-89-75</a>
                <ul class="list-inline pull-right">
                  <li><a href="#" class="fa-facebook"></a></li>
                  <li><a href="#" class="fa-pinterest-p"></a></li>
                  <li><a href="#" class="fa-twitter"></a></li>
                  <li><a href="#" class="fa-google-plus"></a></li>
                  <li><a href="#" class="fa-instagram"></a></li>
                </ul>
              </div>
            </div>
            <div class="rd-navbar-inner">

              <!--RD Navbar Panel-->
              <div class="rd-navbar-panel">

                <!--RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar" class="rd-navbar-toggle"><span></span></button>
                <!--END RD Navbar Toggle-->

                <!--RD Navbar Brand-->
                <div class="rd-navbar-brand"><a href="index.html" class="brand-name">
                    EAL
                  </a>
                </div>
                <!--END RD Navbar Brand-->
              </div>
              <!--END RD Navbar Panel-->
            @include('layouts._links')
            </div>
          </nav>
        </div>
        <!--END RD Navbar-->
        <section>
          <!--Swiper-->
          <div data-autoplay="5000" data-slide-effect="fade" data-loop="false" class="swiper-container swiper-slider">
            <div class="jumbotron text-center">
              <h1>Services<small>About Our Services</small></h1>
              <p class="big"></p>
            </div>
            <div class="swiper-wrapper">
              <div data-slide-bg="images/header-1.jpg" class="swiper-slide">
                <div class="swiper-slide-caption"></div>
              </div>
              <div data-slide-bg="images/header-2.jpg" class="swiper-slide">
                <div class="swiper-slide-caption"></div>
              </div>
              <div data-slide-bg="images/header-3.jpg" class="swiper-slide">
                <div class="swiper-slide-caption"></div>
              </div>
              <div data-slide-bg="images/header-4.jpg" class="swiper-slide">
                <div class="swiper-slide-caption"></div>
              </div>
            </div>
          </div>
        </section>
      </header>