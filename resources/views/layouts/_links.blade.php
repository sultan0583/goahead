	<div class="rd-navbar-nav-wrap">

		<a href="/lang" class="material-icons-language">
		</a>
		<!--RD Navbar Search-->
		<div class="rd-navbar-search">
			<form action="search.php" method="GET" class="rd-navbar-search-form">
				<label class="rd-navbar-search-form-input">
					<input type="text" name="s" placeholder="Search.."
					autocomplete="off">
				</label>
				<button type="submit" class="rd-navbar-search-form-submit"></button>
			</form><span class="rd-navbar-live-search-results"></span>
			<button data-rd-navbar-toggle=".rd-navbar-search, 
			.rd-navbar-live-search-results" class="rd-navbar-search-toggle">

		</button>
	</div>
	<!--END RD Navbar Search-->

	<!--RD Navbar Nav-->
	<ul class="rd-navbar-nav">
		<li @if(request()->is('/')) {{ "class = active" }} @endif >
					
					<a href="./">Home</a>
		</li>
		<li @if(request()->is('services')) {{ "class = active" }} @endif ><a href="#">Services</a>

			<!--RD Navbar Dropdown-->
			<ul class="rd-navbar-dropdown">
				<li class="active"><a href="/services">Service 1</a></li>
				<li><a href="/services">Service 2</a></li>
			</ul>
			<!--END RD Navbar Dropdown-->

		</li>
		<li @if(request()->is('about')) {{ "class = active" }} @endif ><a href="/about">About Us</a>
		</li>
		<li @if(request()->is('gallery')) {{ "class = active" }} @endif ><a href="/gallery">Gallery</a>
		</li>
		
		<li @if(request()->is('mission')) {{ "class = active" }} @endif ><a href="#">Mission</a>
			<ul class="rd-navbar-dropdown">
				<li><a href="/mission">Mission 1</a></li>
				<li><a href="/mission">Mission 2</a></li>
			</ul>
		</li>
		<li @if(request()->is('contact')) {{ "class = active" }} @endif ><a href="/contact">Contact Us</a>
			
		</li>
		
	</ul>
	<!--END RD Navbar Nav-->
</div>