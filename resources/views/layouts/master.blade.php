<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <!--Site Title-->
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <!--Stylesheets-->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">

    <!--Bootstrap-->
    <link rel="stylesheet" href="css/style.css">

    @if(session()->has('lang') && session('lang') == 'ar') 
      <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
      <link rel="stylesheet" href="css/rtl.css">
    @endif
    

<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/.."><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script><[endif]-->
  </head>

  <body>
    <!--The Main Wrapper-->
    <div class="page">
      {{--
      ========================================================
                              HEADER
      ========================================================
     --}}
        @yield('header');
			
			{{-- 
      ========================================================
                              CONTENT
      ========================================================
       --}}

      @yield('content')
      
      {{-- 
      ========================================================
                              FOOTER
      ========================================================
      --}}
      @include('layouts._footer')

    </div>
    <!--Core Scripts-->
    <script src="js/core.min.js"></script>
    <!--jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <!--Include all compiled plugins (below), or include individual files as needed-->
    <script src="js/bootstrap.min.js"></script>
    <!--Additional Functionality Scripts-->
    <script src="js/script.js"></script>
  </body>
</html>