<header class="page-header dark">

	<!--RD Navbar-->
	<div class="rd-navbar-wrap">
		<nav class="rd-navbar bg-white" 
		data-layout="rd-navbar-fixed" data-hover-on="false" data-stick-up="false" 
		data-sm-layout="rd-navbar-fullwidth" data-md-layout="rd-navbar-static">

			<div class="rd-navbar-top-panel">
				<div class="rd-navbar-inner">
					<button data-rd-navbar-toggle=".list-inline, .fa-envelope, .fa-phone,
					.material-icons-language" class="rd-navbar-collapse-toggle">
					<span></span>
					</button>
					<a href="mailto:#" class="fa-envelope">info@goahead.com.eg</a>
					<a href="callto:#" class="fa-phone">+20 (126) 598-89-75</a>


					<ul class="list-inline pull-right">
						<li><a href="#" class="fa-facebook"></a></li>
						<li><a href="#" class="fa-pinterest-p"></a></li>
						<li><a href="#" class="fa-twitter"></a></li>
						<li><a href="#" class="fa-google-plus"></a></li>
						<li><a href="#" class="fa-instagram"></a></li>
					</ul>
				</div>
			</div>


			<div class="rd-navbar-inner">

				<!--RD Navbar Panel-->
				<div class="rd-navbar-panel">

					<!--RD Navbar Toggle-->
					<button data-rd-navbar-toggle=".rd-navbar" class="rd-navbar-toggle">
						<span></span>
					</button>
					<!--END RD Navbar Toggle-->

					<!--RD Navbar Brand-->
					<div class="rd-navbar-brand">
						<a href="index.html" class="brand-name">
							EAL
						</a>
					</div>
					<!--END RD Navbar Brand-->

				</div>
			<!--END RD Navbar Panel-->
				
			@include('layouts._links')
			
			</div>
		</nav>
	</div>
<!--END RD Navbar-->
<section>
	<!--Swiper-->
	<div class="rd-parallax">
		<div data-speed="0.6" data-type="media" data-url="images/soltan/banner-5.jpg" 
		class="rd-parallax-layer"></div>
		<div data-speed="0.78" data-type="html" data-fade="true" 
		class="well-parallax jumbotron text-center rd-parallax-layer">
		<h1>10 YEARS<small>of EXPERIENCE  IN THE INDUSTRY</small></h1>
		<p class="big">
			Lorem ipsum dolor sit amet, duo malorum vituperata cu, augue simul no nec. Dicam praesent mediocrem est atr<br> possible, and 
vel doctus dolorum admodum at. Eam quas commune deserunt cu, ei sit democritum		</p>
		<div class='btn-group-variant'>
			<a class='btn btn-default round-xl btn-sm' href='#'>SEE ALL PROJECTS</a>
			<a class='btn btn-default round-xl btn-sm' href='#'>about us</a>
		</div>
	</div>
</div>
</section>
</header>